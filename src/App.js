import React from 'react';
import Header from './Components/Header';
import ConverterForm from './Components/ConverterForm';
import Footer from './Components/Footer';

function App() {
  return (
    <div>
      <Header />
      <ConverterForm />
      <Footer />
    </div>
  );
}

export default App;
