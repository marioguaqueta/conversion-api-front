import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
            <header className="row deep-orange valign-wrapper">
                <h1 className="white-text col s12 m12">Forx conversor</h1>
            </header>
        )
    }
}

export default Header
