// import React, { Component } from 'react'



// class ConverterForm extends Component {

//     constructor(props) {
//         super(props);

//         this.state = {
//             valueForce: 0,
//             result: 0,
//             fromUnitForce: "nd"
//         }
//         this.handleChange = this.handleChange.bind(this);
//         this.handleClick = this.handleClick.bind(this);
//     }

//     handleChange(event) {
//         if (event.target.name === "force") {
//             this.setState({ valueForce: event.target.value });
//         }
//         if (event.target.name === "from") {
//             console.log(event.target.value);
//             this.setState({ fromUnitForce: event.target.value });
//         }
//     }

//     async handleClick() {
//         var url = "https://mwup59gvc3.execute-api.us-west-2.amazonaws.com/default/converapi";
       
//         const response = await fetch(url, {
//             method: 'POST',
//             mode:'no-cors',
//             headers: {
//                 // 'Accept': 'application/json',
//                 'Content-Type': 'application/json',
//                 'Access-Control-Allow-Origin': '*',
//             },
//             body: JSON.stringify({
//                 value:this.state.valueForce,
//                 conversionType:this.state.fromUnitForce        
//             }),
//         });
//         // const res = await response.json();

//         // this.setState({ result: res.result });

//     }


//     render() {
//         return (
//             <div className="container">
//                 <div className="row">
//                     <form className="col s12">
//                         <div className="row">
//                             <div className="input-field col s10 offset-s1 m2 offset-m4">
//                                 <input name="force" type="number" value={this.state.valueForce} onChange={this.handleChange}></input>
//                                 <label>Value of force</label>
//                             </div>
//                             <div className="input-field col s10 offset-s1 m3" >
//                                 <select name="from" onChange={this.handleChange} defaultValue={this.fromUnitForce}>
//                                     <option value="nd" >Newton-Dynes</option>
//                                     <option value="dn">Dyne-Newton</option>
//                                     <option value="pd">Pound-Dynes</option>
//                                     <option value="dp">Dynes-Pounds</option>
//                                     <option value="np">Newton-Pounds</option>
//                                     <option value="pn">Pounds-Newton</option>
//                                 </select>
//                                 <label>From</label>
//                             </div>
//                         </div>
//                         <div className="row">
//                             <div className="col s10 offset-s1 m4 offset-m4" onClick={this.handleClick}>
//                                 <a className="waves-effect waves-light btn"><i className="material-icons right">autorenew</i>Convert</a>
//                             </div>
//                         </div>
//                     </form>
//                 </div>
//                 <div className="row">
//                     <div className="col s12 m5 offset-m4">
//                         <div className="card deep-orange lighten-1">
//                             <div className="card-content white-text">
//                                 <span className="card-title">{this.state.result}</span>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//         )
//     }
// }

// export default ConverterForm;


import React, { Component } from 'react'

const url = "https://mwup59gvc3.execute-api.us-west-2.amazonaws.com/default/converapi";

class ConverterForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            valueForce: 0,
            fromUnitForce: "N",
            toUnitForce: "dyn",
            result: 0
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(event) {
        if (event.target.name === "force") {
            this.setState({ valueForce: event.target.value });
        }
        if (event.target.name === "from") {
            this.setState({ fromUnitForce: event.target.value });
        }
        if (event.target.name === "to") {
            this.setState({ toUnitForce: event.target.value });
        }
    }

    async handleClick() {
        if (this.state.fromUnitForce !== this.state.toUnitForce) {
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: this.state.valueForce,
                    from: this.state.fromUnitForce,
                    to: this.state.toUnitForce
                })
            });
            const data = await response.json();
            this.setState({ result: data.result });

        } else {
            alert("'Unit From' and 'Unit to' selector must have diferent values");
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <form className="col s12">
                        <div className="row">
                            <div className="input-field col s10 offset-s1 m2 offset-m4">
                                <input name="force" type="number" value={this.state.valueForce} onChange={this.handleChange}></input>
                                <label>Value of force</label>
                            </div>
                            <div className="input-field col s10 offset-s1 m3">
                                <select name="from" onChange={this.handleChange} defaultValue={'N'}>
                                    <option value="N">Newton</option>
                                    <option value="dyn">Dyne</option>
                                    <option value="lbf">Pound-force</option>
                                </select>
                                <label>From</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s10 offset-s1 m3 offset-m4">
                                <select name="to" onChange={this.handleChange} defaultValue={'dyn'}>
                                    <option value="dyn">Dyne</option>
                                    <option value="N">Newton</option>
                                    <option value="lbf">Pound-force</option>
                                </select>
                                <label>To</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col s10 offset-s1 m4 offset-m4" onClick={this.handleClick}>
                                <a className="waves-effect waves-light btn"><i className="material-icons right">autorenew</i>Convert</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="row">
                    <div className="col s12 m5 offset-m4">
                        <div className="card deep-orange lighten-1">
                            <div className="card-content white-text">
                                <span className="card-title center">{this.state.valueForce} {this.state.fromUnitForce} is equal to {this.state.result} {this.state.toUnitForce}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ConverterForm